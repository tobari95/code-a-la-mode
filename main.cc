#include <iostream>
#include <vector>
#include <array>
#include <map>
#include <string>
#include <queue>
#include <algorithm>
#include <sstream>
#include <tuple>
#include <bitset>
#include <assert.h>

#define DEBUG

std::vector<std::string> SplitString(const std::string& str, char delim) {
  std::vector<std::string> strs;
  std::stringstream ss(str);
  std::string s;

  while (std::getline(ss, s, delim)) {
    if (!s.empty()) {
      strs.push_back(s);
    }
  }
  return strs;
}

static constexpr int kWidth = 11;
static constexpr int kHeight = 7;

enum Cell {
  EMPTY,
  TABLE,
  DISHWASHER,
  WINDOW,
  CHOPPING_BOARD,
  OVEN,
  BLUEBERRY_CRATE,
  ICE_CREAM_CRATE,
  STRAWBERRY_CRATE,
  DOUGH_CRATE
};

using Grid = std::array<std::array<Cell, kHeight>, kWidth>;

enum Element {
  DISH,
  BLUEBERRY,
  ICE_CREAM,
  STRAWBERRY,
  CHOPPED_STRAWBERRY,
  DOUGH,
  CROISSANT,
  CHOPPED_DOUGH,
  RAW_TART,
  TART
};

// Elementの集合を表現します。
// 使用例:
//
//
struct Content {
  int entity;

  static const Content kNone;

  Content(int entity = 0): entity{entity} {}
  Content(const std::string& name) {
    if (name == "NONE") {
      entity = 0;
      return;
    }
    entity = 0;

    std::map<std::string, Element> mapping_table = {
      {"DISH", DISH}, {"BLUEBERRIES", BLUEBERRY}, {"ICE_CREAM", ICE_CREAM},
      {"STRAWBERRIES", STRAWBERRY}, {"CHOPPED_STRAWBERRIES", CHOPPED_STRAWBERRY},
      {"DOUGH", DOUGH}, {"CROISSANT", CROISSANT}, {"CHOPPED_DOUGH", CHOPPED_DOUGH},
      {"RAW_TART", RAW_TART}, {"TART", TART}
    };

    for (std::string str : SplitString(name, '-')) {
      Adds(mapping_table[str]);
    }
  }

  bool operator==(const Content& cnt) const {
    return entity == cnt.entity;
  }
  bool operator!=(const Content& cnt) const {
    return entity != cnt.entity;
  }

  bool Contains(const Content& cnt) const {
    return (entity & cnt.entity) == cnt.entity;
  }
  bool Contains(Element elem) const {
    return (entity >> ((int)elem)) & 1;
  }

  bool Intersects(const Content& cnt) const {
    return entity & cnt.entity;
  }

  void Adds(Element elem) {
    entity |= 1 << ((int)elem);
  }

  int CountElements() const {
    return __builtin_popcount((int)entity);
  }
};

const Content Content::kNone{0};

std::ostream& operator<<(std::ostream& os, const Content& cnt) {
  std::vector<std::tuple<Element, std::string>> elements_and_names{
    {DISH, "DISH"},
    {BLUEBERRY, "BLUEBERRY"},
    {ICE_CREAM, "ICE_CREAM"},
    {STRAWBERRY, "STRAWBERRY"},
    {CHOPPED_STRAWBERRY, "CHOPPED_STRAWBERRY"},
    {DOUGH, "DOUGH"},
    {CROISSANT, "CROISSANT"},
    {CHOPPED_DOUGH, "CHOPPED_DOUGH"},
    {RAW_TART, "RAW_TART"},
    {TART, "TART"}
  };
  for (int i = 0; i < elements_and_names.size(); i++) {
    Element elem;
    std::string name;
    std::tie(elem, name) = elements_and_names[i];

    os << "[" << name << "] " << cnt.Contains(elem) << " ";
  }
  return os;
}

Content UniteContents(const Content& cnt1, const Content& cnt2) {
  return Content{cnt1.entity | cnt2.entity};
}

Content DifferenceContents(const Content& cnt1, const Content& cnt2) {
  return Content{cnt1.entity & (cnt1.entity ^ cnt2.entity)};
}

Content MakeContent(Element elem) {
  return Content{1 << elem};
}

// グリッド上の座標を表現します。
// 使用例:
//
//
struct Square {
  int x;
  int y;

  static const Square kUndefined;

  Square() = default;
  Square(int x, int y): x{x}, y{y} {}

  bool operator==(const Square& sq) const {
    return x == sq.x && y == sq.y;
  }
  bool operator!=(const Square& sq) const {
    return x != sq.x || y != sq.y;
  }
};

const Square Square::kUndefined{-1, -1};

struct Order {
  Content item_content;
  int award;

  Order() = default;
  Order(Content item_content, int award): item_content{item_content}, award{award} {}
};

struct Player {
  Square square;
  Content item_content;

  Player() = default;
  Player(Square square, Content item_content): square{square}, item_content{item_content} {}
};

struct Item {
  Square square;
  Content content;

  Item() = default;
  Item(Square square, Content content): square{square}, content{content} {}
};

enum ActionType {
  MOVE,
  USE,
  WAIT
};

// プレイヤーが行う1ターンの行動を表現します。
// 使用例:
//
struct Action {
  Square square;
  ActionType type;
  std::string message;

  Action() = default;
  Action(Square square, ActionType type, std::string message = ""):
    square{square}, type{type}, message{message} {}

  friend std::ostream& operator<<(std::ostream& os, const Action& act);
};

std::ostream& operator<<(std::ostream& os, const Action& act) {
#ifndef DEBUG
  std::string message = "";
#else
  std::string message = act.message;
#endif

  if (act.type == WAIT) {
    os << "WAIT; " << message;
    return os;
  }
  os << (act.type == USE ? "USE" : "MOVE");
  os << " " << act.square.x << " " << act.square.y << "; " << message;
  return os;
}

namespace Think { Action Agent(); }

namespace Game {

std::vector<Order> all_orders;
Grid grid;
int turns_remaining;
std::array<Player, 2> players;
std::vector<Item> items_on_tables;
std::vector<Order> pending_orders;
Content oven_content;
int oven_timer;

void InitializationInput() {
  int num_all_orders;
  std::cin >> num_all_orders; std::cin.ignore();

  all_orders.resize(num_all_orders);
  for (int i = 0; i < num_all_orders; i++) {
    std::string customer_item_name;
    int customer_award;
    std::cin >> customer_item_name >> customer_award; std::cin.ignore();

    all_orders[i] = Order{Content{customer_item_name}, customer_award};
  }

  std::map<char, Cell> mapping_table = {
    {'.', EMPTY}, {'D', DISHWASHER}, {'#', TABLE},
    {'W', WINDOW}, {'C', CHOPPING_BOARD}, {'O', OVEN},
    {'B', BLUEBERRY_CRATE}, {'I', ICE_CREAM_CRATE},
    {'S', STRAWBERRY_CRATE}, {'H', DOUGH_CRATE}
  };

  for (int y = 0; y < kHeight; y++) {
    std::string kitchen_line;
    std::getline(std::cin, kitchen_line);

    for (int x = 0; x < kWidth; x++) {
      grid[x][y] = mapping_table[kitchen_line[x]];
    }
  }
}

void StepInput() {
  std::cin >> turns_remaining; std::cin.ignore();

  int player_x, player_y;
  std::string player_item_name;
  std::cin >> player_x >> player_y >> player_item_name; std::cin.ignore();

  players[0] = Player{Square{player_x, player_y}, Content{player_item_name}};

  int partner_x, partner_y;
  std::string partner_item_name;
  std::cin >> partner_x >> partner_y >> partner_item_name; std::cin.ignore();

  players[1] = Player{Square{partner_x, partner_y}, Content{partner_item_name}};

  int num_tables_with_items;
  std::cin >> num_tables_with_items; std::cin.ignore();

  items_on_tables.resize(num_tables_with_items);

  for (int i = 0; i < num_tables_with_items; i++) {
    int table_x, table_y;
    std::string item_name;

    std::cin >> table_x >> table_y >> item_name; std::cin.ignore();

    items_on_tables[i] = Item{Square{table_x, table_y}, Content{item_name}};
  }

  std::string oven_content_name;
  std::cin >> oven_content_name >> oven_timer; std::cin.ignore();

  oven_content = Content{oven_content_name};

  int num_orders;
  std::cin >> num_orders; std::cin.ignore();

  pending_orders.resize(num_orders);
  for (int i = 0; i < num_orders; i++) {
    std::string order_item_name;
    int order_award;
    std::cin >> order_item_name >> order_award; std::cin.ignore();

    pending_orders[i] = Order{Content{order_item_name}, order_award};
  }
}

void OutputAction() {
  Action next_action = Think::Agent();
  std::cout << next_action << std::endl;
}
}

namespace Think {

bool IsInside(Square s) {
  return 0 <= s.x && s.x < kWidth && 0 <= s.y && s.y < kHeight;
}

Action NewWaitAction(std::string message) {
  return Action{Square::kUndefined, WAIT, message};
}

// sqから距離1で移動できるマスを列挙する。
std::vector<Square> EnumerateMovableSquares(Square sq) {
  std::vector<Square> squares;

  for (int dx = -1; dx <= 1; dx++) {
    for (int dy = -1; dy <= 1; dy++) {
      if (std::abs(dx) + std::abs(dy) != 1) continue;

      Square nsq{sq.x + dx, sq.y + dy};

      if (IsInside(nsq) && Game::grid[nsq.x][nsq.y] == EMPTY && sq != Game::players[1].square) {
        squares.push_back(nsq);
      }
    }
  }
  return squares;
}

// sqの周囲8近傍を列挙する。
std::vector<Square> EnumerateAdjacentSquares(Square sq) {
  std::vector<Square> squares;
  for (int dx = -1; dx <= 1; dx++) {
    for (int dy = -1; dy <= 1; dy++) {
      if (dx == 0 && dy == 0) continue;

      Square nsq{sq.x + dx, sq.y + dy};

      if (IsInside(nsq)) {
        squares.emplace_back(sq.x + dx, sq.y + dy);
      }
    }
  }
  return squares;
}

// Content周り

Content SingleContent(Element elem) {
  Content cnt;
  cnt.Adds(elem);
  return cnt;
}

Content FullyCookedContent(const Content& content) {
  if (content == SingleContent(DOUGH)) {
    return UniteContents(SingleContent(CROISSANT), SingleContent(TART));
  }
  if (content == SingleContent(CHOPPED_DOUGH) || content == SingleContent(RAW_TART)) {
    return SingleContent(TART);
  }
  if (content == SingleContent(STRAWBERRY)) {
    return SingleContent(CHOPPED_STRAWBERRY);
  }
  return content;
}

// もしバグっていたらこの部分を疑え！！！！
// GitLabと比較すること。
bool IsFullyCookedContent(const Content& content) {
  return content == FullyCookedContent(content);
}

bool DoesHave(Element elem) {
  return Game::players[0].item_content.Contains(elem);
}

bool IsServing(Element elem) {
  return DoesHave(DISH) && DoesHave(elem);
}

std::array<std::array<int, kHeight>, kWidth> distance;

// 幅優先探索における探索の状態を表す構造体。
struct BFSNode {
  Square square;
  int d;
  BFSNode() = default;
  BFSNode(Square square, int d): square{square}, d{d} {}
};

// Game::grid上で幅優先探索をする。
// 距離はdistanceに格納される。未訪問ノードの距離は-1。
void BFS(const std::vector<BFSNode>& initial_nodes) {
  for (int x = 0; x < kWidth; x++) {
    for (int y = 0; y < kHeight; y++) {
      distance[x][y] = -1;
    }
  }

  std::queue<BFSNode> que;

  for (const BFSNode& node : initial_nodes) {
    que.push(node);
    distance[node.square.x][node.square.y] = 0;
  }

  while (!que.empty()) {
    BFSNode node = que.front(); que.pop();

    for (const Square& sq : EnumerateMovableSquares(node.square)) {
      if (distance[sq.x][sq.y] < 0) {
        que.push(BFSNode(sq, node.d + 1));
        distance[sq.x][sq.y] = node.d + 1;
      }
    }
  }
}

// "USE target_sq.x target_sq.y"を成功させるための次の一手を返します。
Action TryToUse(Square target_sq, std::string message = "") {
  Square player_sq = Game::players[0].square;

  std::vector<BFSNode> initial_nodes;
  for (const Square& sq : EnumerateAdjacentSquares(target_sq)) {
    if (Game::grid[sq.x][sq.y] == EMPTY && Game::players[1].square != sq) {
      initial_nodes.emplace_back(sq, 0);
    }
  }

  BFS(initial_nodes);

  if (distance[player_sq.x][player_sq.y] == 0) {
    return Action(Square(target_sq.x, target_sq.y), USE, message);
  }

  // 最大4回動く
  Square current_sq = player_sq;
  for (int i = 0; i < 4; i++) {
    for (const Square& sq : EnumerateMovableSquares(current_sq)) {
      if (distance[current_sq.x][current_sq.y] == distance[sq.x][sq.y] + 1) {
        current_sq = sq;
        break;
      }
    }
    if (current_sq == player_sq) {
      return NewWaitAction("動ける場所がないため待機します。");
    }
  }
  return Action(current_sq, MOVE, message);
}

///
/// @brief 自プレイヤーの周囲8近傍のテーブルのどこかに、今手に持っているものを置く操作を返す。
///
// 置ける場所がなければ何もしない。
///
Action TryToPutItemInNearby() {
  for (const Square& sq : EnumerateAdjacentSquares(Game::players[0].square)) {
    if (Game::grid[sq.x][sq.y] != TABLE || sq == Game::players[1].square) continue;

    bool ok = true;

    for (const Item& item : Game::items_on_tables) {
      if (sq == item.square) {
        ok = false;
        break;
      }
    }
    if (ok) {
      return Action(sq, USE, "物を置くぞい");
    }
  }
  return NewWaitAction("物を置こうとしましたが、近くにおける場所がないため待機します。");
}

Square FindCell(Cell cell) {
  for (int x = 0; x < kWidth; x++) {
    for (int y = 0; y < kHeight; y++) {
      if (Game::grid[x][y] == cell) {
        return Square(x, y);
      }
    }
  }
  return Square::kUndefined;
}

Square FindContentOnTable(Content content) {
  for (const Item item : Game::items_on_tables) {
    if (item.content == content) {
      return item.square;
    }
  }
  if (Game::oven_content == content) {
    return FindCell(OVEN);
  }
  return Square::kUndefined;
}

Square FindElementOnTable(Element elem) {
  return FindContentOnTable(SingleContent(elem));
}

bool IsPartnerCloseToOven() {
  Square partner_sq = Game::players[1].square,
         oven_sq = FindCell(OVEN);
  return std::abs(partner_sq.x - oven_sq.x) + std::abs(partner_sq.y - oven_sq.y) <= 2;
}

int EvaluateContent(Content content) {
  int score = 0;
  if (content.Contains(CROISSANT)) score += 10;
  if (content.Contains(CHOPPED_STRAWBERRY)) score += 5;
  if (content.Contains(TART)) score += 20;
  if (content.Contains(BLUEBERRY)) score += 3;
  if (content.Contains(ICE_CREAM)) score += 3;
  return score;
}

bool CanCompleteOrderInThisState(Content order_content) {
  int num_items_on_tables = Game::items_on_tables.size();

  for (int mask = 0; mask < (1 << num_items_on_tables); mask++) {
    Content content = Game::players[0].item_content;

    bool ng = !order_content.Contains(content);
    for (int i = 0; i < num_items_on_tables; i++) {
      if ((mask >> i) & 1) {
        Content item_content = Game::items_on_tables[i].content;
        ng = ng || content.Intersects(item_content);
        content = UniteContents(content, item_content);

        if (!order_content.Contains(item_content)) {
          ng = true;
        }
      }
    }
    if (ng) continue;

    content.Adds(DISH);
    content.Adds(BLUEBERRY);
    content.Adds(ICE_CREAM);

    if (content.Contains(order_content)) {
      return true;
    }
  }
  return false;
}

std::tuple<int, Content> SelectItemSubsetForOrder(Content order_content) {
  int num_items_on_tables = Game::items_on_tables.size();

  int best_subset = 0,
      best_score = 0;
  Content best_content{0};

  for (int mask = 0; mask < (1 << num_items_on_tables); mask++) {
    Content content{0};
    if (!DoesHave(DOUGH) && !DoesHave(STRAWBERRY) && !DoesHave(CHOPPED_DOUGH) && !DoesHave(RAW_TART)) {
      content = Game::players[0].item_content;
    }

    bool ng = !order_content.Contains(content);
    for (int i = 0; i < num_items_on_tables; i++) {
      if ((mask >> i) & 1) {
        Content item_content = Game::items_on_tables[i].content;
        ng = ng || content.Intersects(item_content);
        content = UniteContents(content, item_content);

        if (!order_content.Contains(item_content)) {
          ng = true;
        }
      }
    }
    if (ng) continue;

    int score = EvaluateContent(content);

    if (order_content.Contains(DISH)) {
      content.Adds(DISH);
    }
    if (order_content.Contains(BLUEBERRY)) {
      content.Adds(BLUEBERRY);
    }
    if (order_content.Contains(ICE_CREAM)) {
      content.Adds(ICE_CREAM);
    }

    if (content == order_content) {
      // 適当に加点
      score += 1000;
    }

    if (score > best_score) {
      best_score = score;
      best_content = content;
      best_subset = mask;
    }
  }
  return std::make_tuple(best_subset, best_content);
}

int Distance(Square sq1, Square sq2) {
  return std::abs(sq1.x - sq2.x) + std::abs(sq1.y - sq2.y);
}

bool ShouldWait() {
  Square oven_sq = FindCell(OVEN);
  if (Distance(Game::players[1].square, oven_sq) <= 2 and Game::players[1].item_content == Content::kNone) {
    return false;
  }
  return Distance(Game::players[0].square, oven_sq) <= 2;
}

Content SelectOrder() {
  int target = 1;
  bool flag = false;
  
  for (int i = 0; i < Game::pending_orders.size(); i++) {
    const Order ord = Game::pending_orders[i];
    
    if (CanCompleteOrderInThisState(ord.item_content)) {
      if (!flag or ord.award > Game::pending_orders[target].award) {
        flag = true;
        target = i;
      }
    }
  }
  return Game::pending_orders[target].item_content;
}

// order_contentに含まれる料理と皿を回収する。
Action TryToRetrieveItems(Content order_content) {
  int subset = std::get<0>(SelectItemSubsetForOrder(order_content)),
      num_items_on_tables = Game::items_on_tables.size();

  // 最初に皿をとる
  if (!DoesHave(DISH)) {
    for (int i = 0; i < num_items_on_tables; i++) {
      const Item item = Game::items_on_tables[i];

      if (((subset >> i) & 1) && item.content.Contains(DISH)) {
        return TryToUse(item.square, "テーブルからDISHを含む料理を取りに行きます。");
      }
    }
    return TryToUse(FindCell(DISHWASHER), "DISHWASHERからDISHを取りに行きます。");
  }

  BFS({BFSNode{Game::players[0].square, 0}});

  Square target_sq = Square::kUndefined;
  int dist = (1 << 28);

  auto Calc = [&](Square sq) {
    int d = -1;
    for (const Square s : EnumerateAdjacentSquares(sq)) {
      if (Game::grid[s.x][s.y] == EMPTY and distance[s.x][s.y] >= 0 and (d < 0 or distance[s.x][s.y] < d)) {
        d = distance[s.x][s.y];
      }
    }
    return d;
  };

  for (int i = 0; i < num_items_on_tables; i++) {
    if ((subset >> i) & 1) {
      const Item& item = Game::items_on_tables[i];

      int d = Calc(item.square);

      if (d >= 0 and dist > d) {
        target_sq = item.square;
        dist = d;
      }
    }
  }


  if (order_content.Contains(BLUEBERRY) && !DoesHave(BLUEBERRY)) {
    int d = Calc(FindCell(BLUEBERRY_CRATE));
    if (d >= 0 and d < dist) {
      target_sq = FindCell(BLUEBERRY_CRATE);
      dist = d;
      // TryToUse(target_sq, "BLUEBERRYを取りに行きます。");
    }
  }
  if (order_content.Contains(ICE_CREAM) && !DoesHave(ICE_CREAM)) {
    int d = Calc(FindCell(ICE_CREAM_CRATE));
    if (d >= 0 and d < dist) {
      target_sq = FindCell(ICE_CREAM_CRATE);
      dist = d;
    }
  }
  if (target_sq == Square::kUndefined)
    return NewWaitAction("なんもわからんなんもわからん");
  return TryToUse(target_sq);
}

Action TryToCookTart() {
  Square chopped_dough_sq = FindElementOnTable(CHOPPED_DOUGH),
         raw_tart_sq = FindElementOnTable(RAW_TART),
         tart_sq = FindElementOnTable(TART),
         oven_sq = FindCell(OVEN);

  if (DoesHave(RAW_TART)) {
    if (Game::oven_content != Content::kNone) {
      if (ShouldWait()) {
        return NewWaitAction("TARTを作ろうとしましたが、オーブンが使えないため待機しています。");
      } else {
        return TryToUse(oven_sq, "RAW TARTをどうにかしたいのでオーブンに近づきます");
      }
    } else {
      return TryToUse(FindCell(OVEN), "TARTを焼きに行きます。");
    }
  }
  if (DoesHave(CHOPPED_DOUGH)) {
    return TryToUse(FindCell(BLUEBERRY_CRATE), "RAW TARTを作りに行きます。");
  }
  if (DoesHave(DOUGH)) {
    return TryToUse(FindCell(CHOPPING_BOARD), "DOUGHを切りに行きます。");
  }

  // 何も持っていない
  if (raw_tart_sq != Square::kUndefined) {
    if (raw_tart_sq != oven_sq) {
      return TryToUse(raw_tart_sq, "RAW TARTを取りに行きます。");
    } else if (ShouldWait()) {
      return TryToUse(oven_sq, "オーブン待ち");
    } else if (Distance(Game::players[0].square, oven_sq) > 2) {
      return TryToUse(oven_sq, "オーブンに近く");
    }
  }
  if (chopped_dough_sq != Square::kUndefined) {
    return TryToUse(chopped_dough_sq, "CHOPPED DOUGHを取りに行きます。");
  }
  return TryToUse(FindCell(DOUGH_CRATE), "DOUGHを取りに行きます。");
}

Action TryToCookCroissant() {
  if (Game::oven_content != Content::kNone) {
    if (ShouldWait()) {
      return NewWaitAction("CROISSANTを作ろうとしましたが、オーブンが使えないため待機しています。");
    } else {
      return TryToUse(FindCell(OVEN), "DOUGHをどうにかしたいのでオーブンに近づきます");
    }
  }
  if (DoesHave(DOUGH)) {
    return TryToUse(FindCell(OVEN), "CROISSANTを作りに行きます。");
  }
  return TryToUse(FindCell(DOUGH_CRATE), "CROISSANTの材料を取りに行きます。");
}

Action TryToCookChoppedStrawberry() {
  if (DoesHave(STRAWBERRY)) {
    return TryToUse(FindCell(CHOPPING_BOARD), "STRAWBERRYを切りに行きます。");
  }
  return TryToUse(FindCell(STRAWBERRY_CRATE), "STRAWBERRYを取りに行きます。");
}

bool ShouldCollectFoodInOven() {
  return Game::oven_content != Content::kNone && IsFullyCookedContent(Game::oven_content);
}

Action TryToCollectFoodInOven() {
  return TryToUse(FindCell(OVEN), "オーブンにある料理を回収します。");
}

Action TryToCookFoods(Content order_content) {
  Content player_content = Game::players[0].item_content;

  int subset;
  Content content;
  std::tie(subset, content) = SelectItemSubsetForOrder(order_content);

  auto IsRequired = [&](Element elem) {
    return order_content.Contains(elem) && !content.Contains(elem);
  };

  // 既に何かを持っているのなら、それを優先して完成させるようにします。
  if (player_content != Content::kNone) {
    if ((IsRequired(TART) && DoesHave(DOUGH)) || DoesHave(CHOPPED_DOUGH) || DoesHave(RAW_TART)) {
      Action act = TryToCookTart();
      if (act.type != WAIT or !IsPartnerCloseToOven()) {
        return act;
      }
    }
    if (IsRequired(CROISSANT) && DoesHave(DOUGH)) {
      Action act = TryToCookCroissant();
      if (act.type != WAIT or !IsPartnerCloseToOven()) {
        return act;
      }
    }
    if (IsRequired(CHOPPED_STRAWBERRY) && DoesHave(STRAWBERRY)) {
      return TryToCookChoppedStrawberry();
    }
  }

  if (IsRequired(TART)) {
    return TryToCookTart();
  }
  if (IsRequired(CROISSANT)) {
    return TryToCookCroissant();
  }
  if (IsRequired(CHOPPED_STRAWBERRY)) {
    return TryToCookChoppedStrawberry();
  }
  return NewWaitAction("有り得ない挙動です。 in TryToCookFoods");
}

// テーブル上に存在している料理だけでは注文の品を作れないかどうかを判定します。
bool ShouldCookFoods(Content order_content) {
  return std::get<1>(SelectItemSubsetForOrder(order_content)) != order_content;
}

// 注文の品を作る上で今手に持っているものが邪魔かどうかを判定します。
bool DoesHaveObstacle(Content order_content) {
  Content player_content = Game::players[0].item_content;

  if (CanCompleteOrderInThisState(order_content)) {
    return false;
  }

  int subset;
  Content content;
  std::tie(subset, content) = SelectItemSubsetForOrder(order_content);

  // 今持っているもののせいで完成品を作れない
  if (content == order_content) {
    std::cerr << "今持っているもののせいで完成品を作れません。" << std::endl;
    return true;
  }

  if (player_content != Content::kNone && IsFullyCookedContent(player_content)) {
    return true;
  }

  Content diff_content = DifferenceContents(order_content, content);

  // DISHか、関係ないものの素材を持っていたらアウトということにする
  return player_content.Contains(DISH) || !diff_content.Intersects(FullyCookedContent(player_content));
}

// 手に持っている物を置いた方がスコアの期待値が高くなるかどうか判定します。
bool ShouldPutItem(Content order_content) {
  Content player_content = Game::players[0].item_content;

  // 優先度:最高 既に完成品を持っているか何も持っていない時
  if (player_content == order_content || player_content == Content::kNone) {
    return false;
  }

  // 優先度:高 オーブンの料理を放置しておくとまずい時
  if (Game::oven_content == SingleContent(CROISSANT) || Game::oven_content == SingleContent(TART)) {
    return true;
  }

  // 手に持っているものの上位互換があったら捨てる
  for (const Item& item : Game::items_on_tables) {
    if (!order_content.Contains(item.content)) continue;

    if (item.content.Contains(player_content) && item.content != player_content) {
      return true;
    }
  }
  return DoesHaveObstacle(order_content);
}

Action Agent() {
  Content order_content = SelectOrder();

  std::cerr << "目標は" << order_content << std::endl;
  std::cerr << "手持は" << Game::players[0].item_content << std::endl;

  if (ShouldPutItem(order_content)) {
    return TryToPutItemInNearby();
  }
  if (ShouldCollectFoodInOven()) {
    return TryToCollectFoodInOven();
  }
  if (ShouldCookFoods(order_content)) {
    return TryToCookFoods(order_content);
  }
  if (Game::players[0].item_content != order_content) {
    return TryToRetrieveItems(order_content);
  }
  return TryToUse(FindCell(WINDOW));
}
}

int main() {
  Game::InitializationInput();

  while (true) {
    Game::StepInput();
    Game::OutputAction();
  }
  return 0;
}
